//////////////////////////////////////////////////////////////////////////////
Class @NonLinExplTerm
//////////////////////////////////////////////////////////////////////////////
{
  Text identify;
  Serie driver;
  Serie output;
  Set parameters;
  Set fullDomainConstraints;
  Set startConstraints;
  Set stopConstraints;
  NameBlock inputGen;
  Set jumps;
  Set base;
  
  ////////////////////////////////////////////////////////////////////////////
  Text _getTerm(Real c, Text paramName)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Text idc = paramName;
    Case(EQ(c, 0),  "",
      EQ(c, 1), "+"+idc,
      EQ(c,-1), "-"+idc,
      GT(c, 0), "+"+idc+"*"<<c,
      LT(c, 0), "-"+idc+"*"<<(Real -c))
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Text GetEvalFunDerExpr(Real d, Real x, Text paramPrefix, Text paramSufix)
  ////////////////////////////////////////////////////////////////////////////
  {
    SetSum(For(1,inputGen::_.freeDeg,Text(Real k) 
    { 
      Real F = inputGen::Fdk(d,k,x);
      Text idc = paramPrefix+parameters[k]+paramSufix;
      _getTerm(F,idc)
    }))    
  };  

  ////////////////////////////////////////////////////////////////////////////
  Text GetInequations(Text paramPrefix, Text paramSufix)
  ////////////////////////////////////////////////////////////////////////////
  {
    SetSum(For(1,VRows(inputGen::_.A),Text(Real j)
    {
      Real a = (VMatDat(inputGen::_.a,j,1)+1)-1;
      SetSum(For(1,inputGen::_.freeDeg, Text(Real k)
      {
        Real A = (VMatDat(inputGen::_.A,j,k)+1)-1;
        Text idc = paramPrefix+parameters[k]+paramSufix;
        _getTerm(A,idc)
      }))+"<="<<a+";\n"
    }))
  };

  ////////////////////////////////////////////////////////////////////////////
  Text GetPriors(Text paramPrefix, Text paramSufix)
  ////////////////////////////////////////////////////////////////////////////
  {
    SetSum(For(1,VRows(inputGen::_.C),Text(Real j)
    {
      Real g = (VMatDat(inputGen::_.c,j,1)+1)-1;
      SetSum(For(1,inputGen::_.freeDeg, Text(Real k)
      {
        Real G = (VMatDat(inputGen::_.C,j,k)+1)-1;
        Text idc = paramPrefix+parameters[k]+paramSufix;
        _getTerm(G,idc)
      }))+"~"<<g+";\n"
    }))
  };  
   
  ////////////////////////////////////////////////////////////////////////////
  Static @NonLinExplTerm Create(
    @InputGen ig,
    Text id, 
    Serie x,
    Serie y,
    Real weakSigmaPrior, 
    Real degree, 
    Set fullDomainConstraints, 
    Set startConstraints, 
    Set stopConstraints,
    Set pointPriors) 
  ////////////////////////////////////////////////////////////////////////////
  {
    WriteLn("TRACE [@NonLinExplTerm Create] "+id);
    Set parameters = For(1,degree,Text(Real k) { id+".D"<<k });
    
    Real ig::initialize(?);

    Set EvalSet(fullDomainConstraints,Real(Text constraint)
    {
      Eval("ig::is"+constraint+"AtMesh(ig::_.freeDeg)")
    });
    Set EvalSet(startConstraints,Real(Text constraint)
    {
      Eval("ig::is"+constraint+"At(ig::_.lower)")
    });
    Set EvalSet(stopConstraints,Real(Text constraint)
    {
      Eval("ig::is"+constraint+"At(ig::_.upper)")
    });
    Real priorProb = 2*DistNormal(1)-1;
    Set EvalSet(pointPriors,Real(Set rec)
    {
      Text id = ToLower(rec->Name);
      Text id3 = Sub(id,1,3);
      Case(
      id3=="max", ig::addPrior.max   (rec->Nu, rec->Sigma, priorProb),
      id3=="min", ig::addPrior.min   (rec->Nu, rec->Sigma, priorProb),
      id3=="inf", ig::addPrior.inflex(rec->Nu, rec->Sigma, priorProb))
    });
    Real If(IsFinite(weakSigmaPrior),
      ig::addPrior.b.all(weakSigmaPrior, priorProb));
    Set jumps = If(True, Copy(Empty), {
      Serie yd = y - AvrS(y);
      Date fst = Max(First(y),First(x));
      Date lst = Min(Last (y),Last (x));
      Matrix XY = Tra(SerSetMat([[x,yd]],fst,lst));
      Set noMissIdx = MatQuery::SelectRowsFullKnown(Mat2VMat(XY*Col(1,1)));
      Matrix XYk  = SubRow(XY,noMissIdx);
      Matrix order = Sort(XYk,[[2]]);
      Matrix XYs = PivotByRows(XYk,order);
      Matrix X  = SubCol(XYs,[[1]]);
      Matrix Y  = SubCol(XYs,[[2]]);
      Real parts = 2*ig::_.freeDeg;
      Real step = 1/(parts);
      Set q = Range(step,1.00001-step,step);
      Set points =
        [[ig::_.lower]]<<
        MatSet(Tra(Quantile(X,SetCol(q))))[1]<<
        [[ig::_.upper]];
        Set jumps = For(2,parts+1,Set(Real j){
        Real a = points[j-1];
        Real b = points[j ];
        
        Set sel = MatQuery::SelectRowsInInterval(Mat2VMat(X), True, a, b, True);
        Matrix Xj = SubRow(X,sel);
        Matrix Yj = SubRow(Y,sel);
        Real jump = MatMax(Yj)-MatMin(Xj);
      //Real ig::addConstraint.jump(0,a,b,jump);
        Real ig::addPrior.jump(0,a,b,0,jump,priorProb);
        [[a,b,jump]]
      })      
    });
    
     
    Matrix Fx = VMat2Mat(ig::F_(Mat2VMat(SerSetMat([[x]]),1)));
    Set base = MatSerSet(Tra(Fx),Dating(x),First(x));    
    Set For(1,degree,Real(Real k)
    {
      Serie PutName(parameters[k],base[k]); 
      k
    });
    @NonLinExplTerm nlet =[[
      Text identify = id;
      Serie driver = x;
      Serie output = y;
      Set parameters = parameters;
      Set fullDomainConstraints  = fullDomainConstraints;
      Set startConstraints = startConstraints;
      Set stopConstraints  = stopConstraints;
      NameBlock inputGen = ig;
      Set base = base;
      Set jumps = jumps
    ]]
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Static @NonLinExplTerm Laguerre(
    Text id, 
    Serie x,
    Serie y,
    Real weakSigmaPrior, 
    Real degree, 
    Set fullDomainConstraints, 
    Set startConstraints, 
    Set stopConstraints,
    Set pointPriors) 
  ////////////////////////////////////////////////////////////////////////////
  {
    LinAproxNonLinReg::@Laguerre ig = [[
      Real _.minDeg = 1;
      Real _.maxDeg = degree;
      Real _.maxDer = degree;
      Real _.lower = MinS(x);
      Real _.upper = MaxS(x)
    ]];
    @NonLinExplTerm::Create(ig,id,x,y,weakSigmaPrior,degree,
      fullDomainConstraints, startConstraints, stopConstraints, pointPriors)
  }
  
};


